package com.example.vocabularycontroller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VocabularyControllerApplication {

    public static void main(String[] args) {
        SpringApplication.run(VocabularyControllerApplication.class, args);
    }

}
